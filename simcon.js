/* globals Chart:false, feather:false */

(function () {
    'use strict'

    feather.replace()
    
    // Make isomorphic size spectra
    var sizeBins = [];
    for (var i = 1;i <= 1000; i+=20) {
        sizeBins.push(i);
    }
    var conData = [];
    for (var i=0; i < sizeBins.length; i++) {
        var tempCon = 10**6 * (4.0 / 3.0 * Math.PI * (sizeBins[i] / 2) ** 3) ** (-1.0);
        conData.push([sizeBins[i],tempCon]);
    }
  
    var chart = new Highcharts.Chart({

        chart: {
            renderTo: 'container',
            animation: false
        },
        
        title: {
            text: 'Particle Size Spectrum'
        },

        xAxis: {
            tickInterval: 1,
            type: 'logarithmic'
            
        },
        
        yAxis: {
            tickInterval: 1,
            type: 'logarithmic'
        },

        plotOptions: {
            series: {
                point: {
                    events: {

                        drag: function (e) {
                            // Returning false stops the drag and drops. Example:
                            /*
                            if (e.newY > 300) {
                                this.y = 300;
                                return false;
                            }
                            */

                            $('#drag').html(
                                'Dragging <b>' + this.series.name + '</b>, <b>' + e.x + '</b> to <b>' + Highcharts.numberFormat(e.y, 2) + '</b>');
                        },
                        drop: function (e) {
                            $('#drop').html(
                                'In <b>' + this.series.name + '</b>, <b>' + e.x + '</b> was set to <b>' + Highcharts.numberFormat(this.y, 2) + '</b>');
                        }
                    }
                },
                stickyTracking: false
            },
            column: {
                stacking: 'normal'
            },
            line: {
                cursor: 'ns-resize'
            }
        },

        tooltip: {
            yDecimals: 2
        },

        series: [ {
            data: conData,
            draggableY: true
        }]

    });

  
}())

